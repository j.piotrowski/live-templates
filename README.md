# Live templates

Live templates (code snippets) for JetBrains' IDEs

For now we have the following templates:

- React component
- Storybook stories
- Styled component

## Installation

[How to import Live Templates in Jetbrains documentation](https://www.jetbrains.com/help/idea/sharing-live-templates.html#import)

Generally import 'settings' directory from that repository to your IDE.
 
## Using

Our Live Templates group is named 'ReactMMSStack' and if you want to use that in some file you have to write name and confirm with enter.

## Contributing

If you have your own ideas create new branch and upload your settings
